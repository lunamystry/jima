{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module Jima.Types where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Data.Aeson.Types
import           Data.List            (intercalate)
import           Data.Time

data Config = Config
  { dir :: FilePath
  , ext :: String
  } deriving (Show)

type AppConfig = MonadReader Config
data AppError = IOError String
              | ParseError String

newtype App a = App
  { runApp :: ReaderT Config (ExceptT AppError IO) a
  } deriving (Monad, Functor, Applicative, AppConfig, MonadIO, MonadError AppError)


type Name = String
type WorkoutDirectory = FilePath

data Workout = Workout Name String [Group]
instance Show Workout where
    show (Workout name d groups) = name <> "\n" <> d <> "\n\n" <> intercalate "\n\n" (map show groups)
instance ToJSON Workout where
    toJSON (Workout n d gs) = object ["name" .= n, "description" .= d, "groups" .= gs]

desc :: Integer -> Effort -> String
desc gid ef = show gid <> " " <> show ef <> " "

-- a set, when there is more than 1 exercise it is a super set
data Group = Group Integer Effort [Exercise]
instance Show Group where
    show (Group g e [])   = desc g e <> " <no exercises>"
    show (Group g e [ex]) = desc g e <> show ex
    show (Group g e exs)  = desc g e <> "\n\t" <> intercalate "\n\t" (map show exs)
instance ToJSON Group where
    toJSON (Group g e exs) = object ["gid" .= g, "effort" .= e, "exercises" .= exs]

type Weight = Int
data Log = Log UTCTime Weight Effort

data Exercise = Exercise Name Effort
instance Show Exercise where
    show (Exercise name effort) = name <> " " <> show effort
instance ToJSON Exercise where
    toJSON (Exercise n e) =  object ["name" .= n, "effort" .= e]

data Effort = Reps Integer
            | Seconds Integer
instance Show Effort where
    show (Reps x)    = "x" <> show x
    show (Seconds x) = show x <> "s"
instance ToJSON Effort where
    toJSON (Reps x)    =  object ["type" .= String "reps" , "value" .= x]
    toJSON (Seconds x) =  object ["type" .= String "seconds" , "value" .= x]
