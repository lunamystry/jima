{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module Jima.Parse where


import           Control.Monad              (void)
import           Data.Void
import           Jima.Types
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

parseWorkout :: Name -> String -> Either String Workout
parseWorkout name wStr = case parse workoutP name wStr of
  Left  err -> Left $ name ++ errorBundlePretty err
  Right ex  -> checkNames name ex

checkNames :: Name -> Workout -> Either String Workout
checkNames n (Workout wn d gs) = if n == wn
  then Right $ Workout wn d gs
  else Left $ "The name '" ++ n ++ "' does not match '" ++ wn ++ "'"

workoutP :: Parser Workout
workoutP = do
  name <- nameP
  space
  description <- descriptionP
  space
  groups <- groupP `sepBy` space
  eof
  return $ Workout name description groups

lineComment :: Parser ()
lineComment = L.skipLineComment "#"

descriptionP :: Parser String
descriptionP = L.lineFold scn
  $ \sc' -> let ps = nameP `sepBy1` try sc' in unwords <$> ps <* scn

scn :: Parser ()
scn = L.space space1 lineComment empty

sc :: Parser ()
sc = L.space (void $ some (char ' ' <|> char '\t')) lineComment empty

integer :: Parser Integer
integer = lexeme L.decimal

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

nameP :: Parser Name
nameP = lexeme (some (printChar)) <?> "name"

groupP :: Parser Group
groupP = (try singlelineGroupP <|> multilineGroupP) <?> "group"

singlelineGroupP :: Parser Group
singlelineGroupP = do
  gid       <- integer
  effort    <- effortP
  exercises <- exerciseP
  scn
  return $ Group gid effort [exercises]

multilineGroupP :: Parser Group
multilineGroupP = L.indentBlock scn p
 where
  p = do
    gid <- integer
    e   <- effortP
    return (L.IndentSome Nothing (return . (Group gid e)) exerciseP)

exerciseP :: Parser Exercise
exerciseP = do
  name   <- someTill printChar $ try $ lookAhead end
  _      <- count 1 (char ' ')
  effort <- effortP
  return $ Exercise name effort
  where end = char ' ' >> effortP

effortP :: Parser Effort
effortP = lexeme (secondsP <|> repsP) <?> "effort"

secondsP :: Parser Effort
secondsP = do
  value <- integer
  _     <- char 's'
  return $ Seconds value

repsP :: Parser Effort
repsP = do
  _     <- char 'x'
  value <- integer
  return $ Reps value
