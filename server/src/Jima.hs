{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Jima
  ( get
  , getAll
  , run
  , module Jima.Types
  )
where

import qualified Control.Exception    as E
import           Control.Monad.Except
import           Control.Monad.Reader
import qualified Data.Bifunctor       as BF
import           Jima.Parse           (parseWorkout)
import           Jima.Types
import           System.FilePath      (takeBaseName, (<.>), (</>))
import           System.FilePath.Glob (compile, globDir1)

get :: Name -> App Workout
get name =
  asks fileName
    >>= loadContent
    >>= either throwError workout
    >>= either throwError return
 where
  fileName c = dir c </> name <.> ext c

  loadContent :: FilePath -> App (Either AppError String)
  loadContent n = BF.first (IOError . show) <$> liftIO (safeReadFile n)

  workout :: String -> App (Either AppError Workout)
  workout str = return $ BF.first ParseError $ parseWorkout name str

getAll :: App [Workout]
getAll = do
  config <- ask
  names  <- liftIO $ getAllNames (dir config) (ext config)
  sequence $ get <$> names

run :: App a -> Config -> IO (Either AppError a)
run a c = runExceptT (runReaderT (runApp a) c)

-- Helpers

safeReadFile :: FilePath -> IO (Either E.IOException String)
safeReadFile = E.try . readFile

getAllNames :: FilePath -> String -> IO [Name]
getAllNames d e = fileNames >>= baseNames
 where
  fileNames = globDir1 (compile $ "*." ++ e) d
  baseNames = return . map takeBaseName
