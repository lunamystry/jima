{ pkgs ? import <nixpkgs> {} }:
with pkgs;
with haskellPackages;

let
  cabal2nix = src: pkgs.runCommand "cabal2nix" {
    buildCommand = ''
        cabal2nix file://"${builtins.filterSource (path: type: path != ".git") src}" > $out
    '';
    buildInputs = [
        pkgs.cabal2nix
    ];
  } "";
  jima = callPackage (cabal2nix ./.) {};
in 
stdenv.mkDerivation rec {
  name = "jima";
  src = ./.;
  buildInputs = [
    pkgs.zlib
    jima
  ];
  installPhase = ''
    mkdir -p $out/
    cp -r ${jima}/* $out
  '';
}
