{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Config ( getConfig, Options(..)) where

import           Data.Semigroup      ((<>))
import           Jima
import           Options.Applicative


data Options = Options
  { config          :: Config
  , clientDirectory :: FilePath
  }

parseOptions :: Parser Options
parseOptions = Options <$> parseConfig <*> parseDir
 where
  parseConfig :: Parser Config
  parseConfig = Config <$> parseDirectory <*> parseExtension
   where
    parseDirectory :: Parser FilePath
    parseDirectory =
      strOption
        $  short 'd'
        <> long "directory"
        <> metavar "dir"
        <> help "Directory that has the workouts"
        <> value "workouts"
        <> showDefault
    parseExtension :: Parser FilePath
    parseExtension =
      strOption
        $  short 'e'
        <> long "extension"
        <> metavar "ext"
        <> help "The extension for the workout files"
        <> value "wkt"
        <> showDefault
  parseDir :: Parser FilePath
  parseDir = argument str (metavar "client-directory" <> help "The directory that")


getConfig :: IO Options
getConfig = execParser (withInfo parseOptions "JIMA")
  where withInfo opts h = info (helper <*> opts) $ header h
