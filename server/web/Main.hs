{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeOperators              #-}

module Main where

import           Config
import           Control.Monad.Except
import           Data.ByteString.Lazy.UTF8      (fromString)
import           Jima                           hiding (run)
import qualified Jima                           (run)
import           Network.Wai
import           Network.Wai.Application.Static
import           Network.Wai.Handler.Warp
import           Servant
import           WaiAppStatic.Types

type API
     = "api" :> "workouts"  :> Get '[JSON] [Workout]
  :<|> "api" :> "workouts"  :> Capture "name" String :> Get '[JSON] Workout
  :<|> Raw

-- Handlers

serverClient :: String -> ServerT Raw m
serverClient clientDir =
    serveDirectoryWith (mySettings clientDir)
      where
        mySettings root = ds { ssLookupFile = lookupFile }
          where
          ds = defaultFileServerSettings root
          lookupFile p = ssLookupFile ds p >>= maybeLookupIndex [unsafeToPiece "index.html"]
          maybeLookupIndex _ (LRFile f)     = return $ LRFile f
          maybeLookupIndex _ (LRFolder f)   = return $ LRFolder f
          maybeLookupIndex index LRNotFound = ssLookupFile ds index


server :: String -> ServerT API App
server clientDir = getAll
    :<|> get
    :<|> serverClient clientDir

api :: Proxy API
api = Proxy

nt :: Config -> App a -> Handler a
nt c a = liftIO (Jima.run a c) >>= either renderError return
 where
  renderError (IOError    e) = throwError err404 { errBody = fromString e }
  renderError (ParseError e) = throwError err400 { errBody = fromString e }

app :: Config.Options -> Application
app c = serve api $ hoistServer api (nt $ config c) (server (clientDirectory c))

main :: IO ()
main = do
  o <- getConfig
  putStrLn $ "Running on :8080 serving workouts from: " ++ dir ( config o) ++ " with extension: " ++ ext (config o)
  run 8080 $ app o
