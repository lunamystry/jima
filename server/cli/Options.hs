{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Options
  ( parseOptions
  , Options(..)
  , parseCLI
  )
where

import           Data.Semigroup      ((<>))
import           Jima
import           Options.Applicative

data Options = Options
  { config      :: Config
  , workoutName :: String
  }

parseOptions :: Parser Options
parseOptions = Options <$> parseConfig <*> parseName
 where
  parseConfig :: Parser Config
  parseConfig = Config <$> parseDirectory <*> parseExtension
   where
    parseDirectory :: Parser FilePath
    parseDirectory =
      strOption
        $  short 'd'
        <> long "directory"
        <> metavar "dir"
        <> help "Directory that has the workouts"
        <> value "workouts"
        <> showDefault
    parseExtension :: Parser FilePath
    parseExtension =
      strOption
        $  short 'e'
        <> long "extension"
        <> metavar "ext"
        <> help "The extension for the workout files"
        <> value "wkt"
        <> showDefault
  parseName :: Parser String
  parseName = argument str (metavar "name" <> help "The name of the workout")


parseCLI :: IO Options
parseCLI = execParser (withInfo parseOptions "JIMA")
  where withInfo opts h = info (helper <*> opts) $ header h
