{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Main where

import           Jima       (get, run)
import           Jima.Types
import           Options

renderError :: AppError -> IO ()
renderError (IOError e) = do
  putStrLn "There was an error:"
  putStrLn $ "  " ++ e
renderError (ParseError e) = do
  putStrLn "There was a parsing error:"
  putStrLn $ "  " ++ e

renderWorkout :: Workout -> IO ()
renderWorkout = putStrLn . show

main :: IO ()
main = do
  options <- parseCLI
  let name = workoutName options
  let conf = config options
  run (get name) conf >>= either renderError renderWorkout
