module State exposing (init, update)

import Navigation exposing (Location)
import Model exposing (Model, Msg(..), Route)
import View exposing (parseLocation)
import Workout


-- INIT


init : Location -> ( Model, Cmd Msg )
init location =
    let
        currentRoute =
            parseLocation location

        ( workoutModel, workoutCmd ) =
            Workout.init

        model =
            { route = currentRoute
            , workoutsModel = workoutModel
            }
    in
        ( model, Cmd.map WorkoutMsg workoutCmd )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnLocationChange location ->
            let
                newRoute =
                    parseLocation location
            in
                ( { model | route = newRoute }, Cmd.none )

        WorkoutMsg msg ->
            let
                ( updatedModel, updateCmd ) =
                    Workout.update msg model.workoutsModel
            in
                ( { model | workoutsModel = updatedModel }, Cmd.map WorkoutMsg updateCmd )
