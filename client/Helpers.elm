module Helpers
    exposing
        ( jimaInput
        , Input(..)
        , isEmpty
        , isValid
        )

import Element exposing (Element, below, column, none, el, padding, paddingEach, width, fill, px)
import Element.Input as Input
import Element.Font as Font
import Color exposing (..)
import Verify


jimaInput : List (Element.Attribute msg) -> String -> Input t -> (String -> msg) -> (t -> String) -> Element msg
jimaInput attributes placeholder input onChange asString =
    case input of
        Clean ->
            Input.text attributes
                { text = ""
                , placeholder = Just (Input.placeholder [] (Element.text placeholder))
                , onChange = Just onChange
                , label = Input.labelAbove [] none
                }

        Invalid error ->
            Input.text
                ([ below
                    (el [ Font.size 10, Font.color red ] (Element.text error))
                 ]
                    ++ attributes
                )
                { text = ""
                , placeholder = Just (Input.placeholder [] (Element.text placeholder))
                , onChange = Just onChange
                , label = Input.labelAbove [] none
                }

        Valid value ->
            Input.text attributes
                { text = asString value
                , placeholder = Nothing
                , onChange = Just onChange
                , label = Input.labelAbove [ Font.size 18, paddingEach { bottom = 5, top = 0, left = 0, right = 0 } ] (Element.text placeholder)
                }



-- FORMS


type Input t
    = Clean -- no interaction yet
    | Invalid String -- has an error
    | Valid t -- it is all good


isEmpty : String -> Verify.Validator String (Input a) a
isEmpty error input =
    case input of
        Clean ->
            -- This would be empty
            Err [ error ]

        Invalid error ->
            Err [ error ]

        Valid value ->
            Ok value


isValid : (a -> Result b c) -> a -> Bool
isValid verifier form =
    case verifier form of
        Ok _ ->
            True

        Err _ ->
            False
