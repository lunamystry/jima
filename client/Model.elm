module Model exposing (..)

import Navigation exposing (Location)
import Workout


-- MESSAGES


type Msg
    = OnLocationChange Location
    | WorkoutMsg Workout.Msg



-- MODEL


type alias Model =
    { route : Route
    , workoutsModel : Workout.Model
    }



-- ROUTES


type Route
    = IndexRoute
    | NotFoundRoute
    | WorkoutsRoute
    | WorkoutsAddRoute
