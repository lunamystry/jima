module Exercise
    exposing
        ( Msg
        , Form
        , Exercise
        , add
        , card
        , list
        , update
        , initialForm
        , verify
        , encode
        , decode
        )

import Element
    exposing
        ( Element
        , none
        , column
        , row
        , alignBottom
        , spacing
        , paddingEach
        , el
        , width
        , px
        , fill
        )
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Verify
import Json.Decode as Decode
import Json.Encode as Encode
import Helpers
    exposing
        ( Input(..)
        , isEmpty
        , jimaInput
        )


-- TYPES


type Msg
    = SwitchTo ExerciseType
    | NameChange String
    | NumChange String
    | CommentChange String


type alias Exercise =
    { name : String
    , num : Int
    , exerciseType : ExerciseType
    , comment : Maybe String
    }


type ExerciseType
    = Reps
    | Seconds
    | UnknownType


type alias Form =
    { exerciseType : Input ExerciseType
    , name : Input String
    , num : Input Int
    , comment : Maybe String

    -- Support
    , errors : List String
    , info : List String
    }


verify : Verify.Validator String Form Exercise
verify =
    Verify.ok Exercise
        |> Verify.verify .name (isEmpty "The name must be given")
        |> Verify.verify .num (isEmpty "You must give number of reps/seconds")
        |> Verify.verify .exerciseType (isEmpty "You must say if it is reps or seconds")
        |> Verify.keep .comment



-- INIT


initialForm : Form
initialForm =
    { exerciseType = Clean
    , name = Clean
    , num = Clean
    , comment = Nothing
    , errors = []
    , info = []
    }



-- VIEW


list : List Exercise -> Element Msg
list exercises =
    column [ spacing 10 ]
        (List.map card exercises)


card : Exercise -> Element msg
card exercise =
    row [ spacing 5 ]
        [ el [ Font.size 25 ] (Element.text exercise.name)
        , el [] (effort exercise.num exercise.exerciseType)
        , el [ Font.size 15 ] (comment exercise.comment)
        ]


effort : Int -> ExerciseType -> Element msg
effort num exerciseType =
    case exerciseType of
        Reps ->
            Element.text <| "x" ++ toString num

        Seconds ->
            Element.text <| toString num ++ "s"

        UnknownType ->
            Element.text <| toString num ++ "uhm..."


comment : Maybe String -> Element msg
comment c =
    case c of
        Nothing ->
            Element.text ""

        Just value ->
            Element.text value


add : Form -> Element Msg
add form =
    column
        [ Border.rounded 5
        , spacing 10
        ]
        [ errors form.errors
        , nameInput form.name
        , effortInput form.num form.exerciseType
        , commentInput form.comment
        ]


errors : List String -> Element Msg
errors errorList =
    column []
        (List.map Element.text errorList)


commentInput : Maybe String -> Element Msg
commentInput comment =
    let
        ( value, label, placeholder ) =
            case comment of
                Nothing ->
                    ( ""
                    , Input.labelAbove [] none
                    , Just (Input.placeholder [] (Element.text "Comment"))
                    )

                Just c ->
                    ( c
                    , Input.labelAbove
                        [ Font.size 18
                        , paddingEach { bottom = 5, top = 0, left = 0, right = 0 }
                        ]
                        (Element.text "Comment")
                    , Nothing
                    )
    in
        column []
            [ Input.text []
                { text = value
                , placeholder = placeholder
                , onChange = Just CommentChange
                , label = label
                }
            , none
            ]


effortInput : Input Int -> Input ExerciseType -> Element Msg
effortInput num kind =
    let
        selectedType =
            case kind of
                Clean ->
                    Nothing

                Invalid error ->
                    Nothing

                Valid t ->
                    Just t
    in
        row [ spacing 10 ]
            -- TODO: Find out if it is possible to make width adaptive
            [ jimaInput [ width (px 150) ] "how many..." num NumChange toString
            , Input.radio []
                { selected = selectedType
                , onChange = Just SwitchTo
                , label = Input.labelAbove [] none
                , options =
                    [ Input.option Reps (Element.text "reps")
                    , Input.option Seconds (Element.text "seconds")
                    ]
                }
            ]


nameInput : Input String -> Element Msg
nameInput input =
    jimaInput [] "Exercise name" input NameChange (\v -> v)



-- UPDATE


update : Msg -> Form -> Form
update msg form =
    case msg of
        SwitchTo value ->
            form
                |> updateType value

        NameChange value ->
            form
                |> updateName value

        NumChange value ->
            form
                |> updateNum value

        CommentChange value ->
            form
                |> updateComment value


updateType : ExerciseType -> Form -> Form
updateType newType form =
    { form | exerciseType = Valid newType }


updateName : String -> Form -> Form
updateName value form =
    if String.isEmpty value then
        { form | name = Invalid "An exercise has to have a name" }
    else
        { form | name = Valid value }


updateNum : String -> Form -> Form
updateNum value form =
    if String.isEmpty value then
        { form | num = Invalid "how many of the... uhm..." }
    else
        case String.toInt value of
            Ok intValue ->
                { form | num = Valid intValue }

            Err error ->
                { form | num = Invalid "you didn't enter a number" }


updateComment : String -> Form -> Form
updateComment value form =
    if String.isEmpty value then
        { form | comment = Nothing }
    else
        { form | comment = Just value }



-- SERIALIZATION


encode : Exercise -> Encode.Value
encode form =
    let
        attributes =
            [ ( "name", Encode.string form.name )
            , ( "num", Encode.int form.num )
            , ( "type", exerciseTypeEncoder form.exerciseType )
            , ( "comment", commentEncoder form.comment )
            ]
    in
        Encode.object attributes


exerciseTypeEncoder : ExerciseType -> Encode.Value
exerciseTypeEncoder t =
    case t of
        Reps ->
            Encode.string "reps"

        Seconds ->
            Encode.string "seconds"

        UnknownType ->
            Encode.string "uhm..."


commentEncoder : Maybe String -> Encode.Value
commentEncoder comment =
    case comment of
        Nothing ->
            Encode.string ""

        Just value ->
            Encode.string value


decode : Decode.Decoder Exercise
decode =
    Decode.map4 Exercise
        (Decode.field "name" Decode.string)
        (Decode.field "num" Decode.int)
        (Decode.field "type" Decode.string |> Decode.andThen decodeType)
        (Decode.field "comment" (Decode.nullable Decode.string))


decodeType : String -> Decode.Decoder ExerciseType
decodeType exerciseType =
    let
        exerciseTypeDecoder : String -> ExerciseType
        exerciseTypeDecoder t =
            case t of
                "reps" ->
                    Reps

                "seconds" ->
                    Seconds

                _ ->
                    UnknownType
    in
        Decode.succeed <| exerciseTypeDecoder exerciseType
