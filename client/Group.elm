module Group
    exposing
        ( Msg
        , Group
        , Form
        , initialForm
        , verify
        , add
        , list
        , update
        , encode
        , decode
        )

import Element
    exposing
        ( Element
        , none
        , el
        , row
        , column
        , spacing
        , padding
        , paddingXY
        , width
        , height
        , fill
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Color exposing (..)
import List.Verify exposing (notEmpty)
import Verify
import Json.Decode as Decode
import Json.Encode as Encode
import Exercise
import Helpers
    exposing
        ( Input(..)
        , isEmpty
        , isValid
        , jimaInput
        )


type Msg
    = ExerciseMsg Exercise.Msg
    | SaveExercise Exercise.Form
    | NumChange String
    | SwitchTo SelectedForm


type SelectedForm
    = SelectedNormal
    | SelectedSuper


type alias SuperGroup =
    { num : Int
    , exercises : List Exercise.Exercise
    }


type alias NormalGroup =
    { exercise : Exercise.Exercise
    , num : Int
    }


type Group
    = Super SuperGroup
    | Normal NormalGroup
    | UnknownGroup


type alias SuperFormConstructor =
    { num : Input Int
    , exerciseForm : Exercise.Form
    , exercises : List Exercise.Exercise
    , errors : List String
    , info : List String
    }


type alias NormalFormConstructor =
    { num : Input Int
    , exercise : Exercise.Form
    , errors : List String
    , info : List String
    }


type Form
    = SuperForm SuperFormConstructor
    | NormalForm NormalFormConstructor


verify : Verify.Validator String Form Group
verify form =
    case form of
        NormalForm f ->
            case verifyNormal f of
                Ok group ->
                    Ok <| Normal group

                Err errors ->
                    Err errors

        SuperForm f ->
            case verifySuper f of
                Ok group ->
                    Ok <| Super group

                Err errors ->
                    Err errors


verifyNormal : Verify.Validator String NormalFormConstructor NormalGroup
verifyNormal =
    Verify.ok NormalGroup
        |> Verify.verify .exercise Exercise.verify
        |> Verify.verify .num (isEmpty "Uyokwenza ngaki?")


verifySuper : Verify.Validator String SuperFormConstructor SuperGroup
verifySuper =
    Verify.ok SuperGroup
        |> Verify.verify .num (isEmpty "Uyokwenza ngaki?")
        |> Verify.verify .exercises (notEmpty "The exercise ke?")



-- INIT


initialForm : Form
initialForm =
    initialNormalForm


initialSuperForm : Form
initialSuperForm =
    SuperForm
        { num = Clean
        , exercises = []
        , exerciseForm = Exercise.initialForm
        , errors = []
        , info = []
        }


initialNormalForm : Form
initialNormalForm =
    NormalForm
        { num = Clean
        , exercise = Exercise.initialForm
        , errors = []
        , info = []
        }



-- UPDATE


update : Msg -> Form -> Form
update msg form =
    case msg of
        ExerciseMsg msg ->
            form
                |> updateExerciseForm msg

        NumChange value ->
            form
                |> updateNum value

        SwitchTo selected ->
            changeSelectedForm selected

        SaveExercise exercise ->
            form
                |> saveExercise exercise


changeSelectedForm : SelectedForm -> Form
changeSelectedForm selected =
    case selected of
        SelectedNormal ->
            initialNormalForm

        SelectedSuper ->
            initialSuperForm


saveExercise : Exercise.Form -> Form -> Form
saveExercise exerciseForm form =
    case Exercise.verify exerciseForm of
        Ok exercise ->
            updateExercise exercise form

        Err errors ->
            updateErrors errors form


updateExercise : Exercise.Exercise -> Form -> Form
updateExercise exercise form =
    case form of
        NormalForm _ ->
            form

        SuperForm f ->
            SuperForm
                { f
                    | exercises = (f.exercises ++ [ exercise ])
                    , exerciseForm = Exercise.initialForm
                }


updateErrors : List String -> Form -> Form
updateErrors errors form =
    case form of
        SuperForm f ->
            SuperForm { f | errors = errors }

        NormalForm f ->
            NormalForm { f | errors = errors }


updateNum : String -> Form -> Form
updateNum value form =
    case form of
        SuperForm f ->
            if String.isEmpty value then
                SuperForm { f | num = Invalid "how many of the... uhm..." }
            else
                case String.toInt value of
                    Ok intValue ->
                        SuperForm { f | num = Valid intValue }

                    Err error ->
                        SuperForm { f | num = Invalid "you didn't enter a number" }

        NormalForm f ->
            if String.isEmpty value then
                NormalForm { f | num = Invalid "how many of the... uhm..." }
            else
                case String.toInt value of
                    Ok intValue ->
                        NormalForm { f | num = Valid intValue }

                    Err error ->
                        NormalForm { f | num = Invalid "you didn't enter a number" }


updateExerciseForm : Exercise.Msg -> Form -> Form
updateExerciseForm msg form =
    case form of
        NormalForm form ->
            let
                exerciseForm =
                    Exercise.update msg form.exercise
            in
                NormalForm { form | exercise = exerciseForm }

        SuperForm form ->
            let
                exerciseForm =
                    Exercise.update msg form.exerciseForm
            in
                SuperForm { form | exerciseForm = exerciseForm }



-- VIEW


add : Form -> Element Msg
add form =
    case form of
        NormalForm f ->
            column []
                [ formSelector form
                , normalForm f
                ]

        SuperForm f ->
            column []
                [ formSelector form
                , superForm f
                ]


superForm : SuperFormConstructor -> Element Msg
superForm form =
    let
        ( onPress, buttonTextColor ) =
            if isValid Exercise.verify form.exerciseForm then
                ( Just (SaveExercise form.exerciseForm), white )
            else
                ( Nothing, lightGrey )
    in
        column [ spacing 10 ]
            [ errors form.errors
            , Element.map ExerciseMsg <| Exercise.add form.exerciseForm
            , Input.button
                [ Background.color lightBlue
                , Font.color buttonTextColor
                , Border.rounded 3
                , width fill
                , padding 10
                ]
                { onPress = onPress
                , label = Element.text "add exercise"
                }
            , Element.map ExerciseMsg <| Exercise.list form.exercises
            , jimaInput [] "Repeat" form.num NumChange toString
            ]


normalForm : NormalFormConstructor -> Element Msg
normalForm form =
    column [ spacing 10 ]
        [ errors form.errors
        , Element.map ExerciseMsg <| Exercise.add form.exercise
        , jimaInput [] "Repeat" form.num NumChange toString
        ]


errors : List String -> Element Msg
errors errorList =
    column []
        (List.map Element.text errorList)


formSelector : Form -> Element Msg
formSelector form =
    let
        ( normalColor, superColor ) =
            case form of
                NormalForm _ ->
                    ( lightCharcoal, lightGrey )

                SuperForm _ ->
                    ( lightGrey, lightCharcoal )
    in
        row
            [ Font.color white
            , spacing 10
            ]
            [ Input.button
                [ Background.color normalColor
                , Border.rounded 3
                , Font.color superColor
                , padding 12
                , width fill
                ]
                { onPress = Just (SwitchTo SelectedNormal)
                , label = Element.text "set"
                }
            , Input.button
                [ Background.color superColor
                , Border.rounded 3
                , Font.color normalColor
                , padding 12
                , width fill
                ]
                { onPress = Just (SwitchTo SelectedSuper)
                , label = Element.text "super set"
                }
            ]


list : List Group -> Element Msg
list groups =
    column [ spacing 10 ]
        (List.map card groups)


card : Group -> Element Msg
card g =
    let
        cardSuper group =
            column [ padding 10, Background.color lightGrey, Border.rounded 2 ]
                [ Element.text <| "x" ++ toString group.num
                , el [ paddingXY 10 0 ] (Element.map ExerciseMsg <| Exercise.list group.exercises)
                ]

        cardNormal group =
            row [ spacing 5, padding 2 ]
                [ Element.text <| "x" ++ toString group.num
                , Element.map ExerciseMsg <| Exercise.card group.exercise
                ]
    in
        case g of
            Normal group ->
                cardNormal group

            Super group ->
                cardSuper group

            UnknownGroup ->
                Element.text "Unknown set type"



-- SERIALIZATION


encode : Group -> Encode.Value
encode f =
    case f of
        Super group ->
            let
                attributes =
                    [ ( "type", Encode.string "super" )
                    , ( "num", Encode.int group.num )
                    , ( "exercises", Encode.list (List.map Exercise.encode group.exercises) )
                    ]
            in
                Encode.object attributes

        Normal group ->
            Encode.object
                [ ( "type", Encode.string "normal" )
                , ( "num", Encode.int group.num )
                , ( "exercise", Exercise.encode group.exercise )
                ]

        UnknownGroup ->
            Encode.object
                [ ( "type", Encode.string "unknown" )
                ]


decode : Decode.Decoder Group
decode =
    Decode.field "type" Decode.string
        |> Decode.andThen decodeGroup


decodeGroup : String -> Decode.Decoder Group
decodeGroup groupType =
    case groupType of
        "super" ->
            decodeSuper

        "normal" ->
            decodeNormal

        _ ->
            Decode.succeed UnknownGroup


decodeNormal : Decode.Decoder Group
decodeNormal =
    let
        -- Cause I don't wanna make another type alias SuperGroup.
        makeNormal exercise num =
            { exercise = exercise, num = num }

        normalGroupDecode =
            Decode.map2 makeNormal
                (Decode.field "exercise" Exercise.decode)
                (Decode.field "num" Decode.int)
    in
        Decode.map Normal normalGroupDecode


decodeSuper : Decode.Decoder Group
decodeSuper =
    let
        -- Cause I don't wanna make another type alias SuperGroup.
        makeSuper exercises num =
            { exercises = exercises, num = num }

        superGroupDecode =
            Decode.map2 makeSuper
                (Decode.field "exercises" (Decode.list Exercise.decode))
                (Decode.field "num" Decode.int)
    in
        Decode.map Super superGroupDecode
