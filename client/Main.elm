module Main exposing (..)

import Navigation
import Model exposing (Model, Msg(OnLocationChange))
import State exposing (init, update)
import View exposing (route)


-- MAIN


main : Program Never Model Msg
main =
    Navigation.program OnLocationChange
        { init = init
        , update = update
        , view = route
        , subscriptions = \_ -> Sub.none
        }
