module View
    exposing
        ( route
        , parseLocation
        )

import Html exposing (Html)
import Element
    exposing
        ( Element
        , layout
        , padding
        , spacing
        , html
        , column
        , link
        , centerX
        , centerY
        , el
        )
import Element.Font as Font
import Element.Background as Background
import Color exposing (..)
import Navigation exposing (Location)
import UrlParser exposing (oneOf, map, parseHash, top, Parser, s, string, (</>))
import Model exposing (Model, Msg(..), Route(..))
import Workout


page : Element msg -> Html msg
page body =
    layout [] <|
        column [ Background.color grey ]
            [ head
            , el [ centerX, centerY ] body
            ]


head : Element msg
head =
    link [ padding 10, Font.size 40, centerX ]
        { url = "#/"
        , label = Element.text "Jima"
        }


indexView : Element msg
indexView =
    column [ spacing 20 ]
        [ link [ Font.size 30, centerX, padding 20 ]
            { url = "#/workouts"
            , label = Element.text "workouts"
            }
        , link [ Font.size 30, centerX, padding 20 ]
            { url = "#/add"
            , label = Element.text "add"
            }
        ]


notFoundView : Element msg
notFoundView =
    column [ spacing 10, padding 40 ]
        [ el [ Font.size 50, centerX ] (Element.text "404")
        , link [ padding 20, Font.size 30 ]
            { url = "#/"
            , label = Element.text "home"
            }
        ]



-- ROUTING


route : Model -> Html Msg
route model =
    case model.route of
        IndexRoute ->
            page <| indexView

        NotFoundRoute ->
            page <| notFoundView

        WorkoutsRoute ->
            page <| Element.map WorkoutMsg <| Workout.list model.workoutsModel.workouts

        WorkoutsAddRoute ->
            page <| Element.map WorkoutMsg <| Workout.add model.workoutsModel.form


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map IndexRoute top
        , map IndexRoute (s "index")
        , map WorkoutsRoute (s "workouts")
        , map WorkoutsAddRoute (s "add")
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
