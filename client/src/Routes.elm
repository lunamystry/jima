module Routes exposing (Route(..), toRoute)

import Url
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, string, top)


type Route
    = IndexRoute
    | WorkoutsRoute
    | WorkoutRoute String
    | LogRoute
    | NotFoundRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map IndexRoute top
        , map WorkoutsRoute (s "workouts")
        , map WorkoutRoute (s "workouts" </> string)
        , map LogRoute (s "log")
        ]


toRoute : Url.Url -> Route
toRoute url =
    Maybe.withDefault NotFoundRoute (parse matchers url)
