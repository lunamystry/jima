module Subscriptions exposing (subscriptions)

import Log
import Models exposing (Model)
import Msgs exposing (Msg(..))
import Workout


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map WorkoutMsg (Workout.subscriptions model.workoutsModel)
        , Sub.map LogMsg (Log.subscriptions model.logModel)
        ]
