module Views exposing (view)

import Browser
import Html exposing (Html, a, div, header, li, main_, text, ul)
import Html.Attributes exposing (class, href, title)
import Log
import Models exposing (Errors, Model)
import Msgs exposing (Msg(..))
import Routes exposing (Route(..))
import Workout



-- Cool input: https://codepen.io/egrucza/pen/GgGmZK


view : Model -> Browser.Document Msg
view model =
    let
        ( currentView, routeTitle ) =
            route model
    in
    { title = "Jima | " ++ routeTitle
    , body = [ page currentView [] ]
    }


route : Model -> ( Html Msg, String )
route model =
    case model.route of
        IndexRoute ->
            ( Html.map WorkoutMsg <| Workout.list model.workoutsModel, "index" )

        WorkoutsRoute ->
            ( Html.map WorkoutMsg <| Workout.list model.workoutsModel, "workouts" )

        WorkoutRoute name ->
            ( Html.map WorkoutMsg <| Workout.detail model.workoutsModel name, "workouts - " ++ name )

        LogRoute ->
            ( Html.map LogMsg <| Log.view model.logModel, "log" )

        NotFoundRoute ->
            ( notFound, "404 - Not found" )


page : Html msg -> Errors -> Html msg
page content errs =
    div [ class "page" ]
        [ head
        , errors errs
        , main_ [] [ content ]
        ]


head : Html msg
head =
    header []
        [ a [ href "/" ] [ text "Jima" ]
        ]


errors : List String -> Html msg
errors es =
    let
        error e =
            li [ class "error" ] [ text e ]
    in
    ul [ class "errors" ]
        (List.map error es)


notFound : Html msg
notFound =
    div [ class "not-found" ]
        [ div [ class "not-found__code" ] [ text "404" ]
        , div [ class "not-found__text" ] [ text "NOT FOUND" ]
        , a [ href "/workouts", title "workouts" ] [ text "workouts" ]
        ]
