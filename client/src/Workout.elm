module Workout exposing
    ( Model
    , Msg(..)
    , Workout
    , detail
    , init
    , list
    , subscriptions
    , update
    )

import Group
import Html exposing (Html, a, button, div, span, text)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D



-- TYPES


type alias Workout =
    { name : String
    , description : String
    , groups : List Group.Group
    }


type Msg
    = GotWorkouts (Result Http.Error (List Workout))
    | Activate (Maybe Group.Group)
    | Deactivate


type alias Model =
    { workouts : List Workout
    , errors : List String
    , activeGroup : Maybe Group.Group
    }



-- INIT


init : String -> ( Model, Cmd Msg )
init url =
    ( { workouts = []
      , errors = []
      , activeGroup = Nothing
      }
    , fetchWorkouts url
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotWorkouts result ->
            case result of
                Ok workouts ->
                    ( { model | workouts = workouts }, Cmd.none )

                Err error ->
                    let
                        errors =
                            model.errors ++ [ errorToString error ]
                    in
                    ( { model | errors = errors }, Cmd.none )

        Activate group ->
            ( { model | activeGroup = group }, Cmd.none )

        Deactivate ->
            ( { model | activeGroup = Nothing }, Cmd.none )


errorToString : Http.Error -> String
errorToString e =
    case e of
        Http.BadUrl _ ->
            "Bad url"

        Http.NetworkError ->
            "Network Error"

        Http.Timeout ->
            "The request timedout"

        Http.BadStatus d ->
            "Bad status" ++ String.fromInt d

        Http.BadBody b ->
            "Bad body" ++ b



-- VIEW


list : Model -> Html Msg
list m =
    div [ class "workouts" ]
        (List.map workoutL m.workouts)


workoutL : Workout -> Html Msg
workoutL w =
    div [ class "workout" ]
        [ a [ href ("/workouts/" ++ w.name), class "workout__header" ]
            [ header w ]
        ]


detail : Model -> String -> Html Msg
detail m n =
    div [ class "workout-detail" ]
        [ a [ class "workout-detail__back-button button", href "/workouts" ]
            [ text "〈list" ]
        , findWorkout m.workouts n |> workoutD m.activeGroup
        ]


workoutD : Maybe Group.Group -> Maybe Workout -> Html Msg
workoutD ag mw =
    case mw of
        Nothing ->
            div [] [ text "There is no workout" ]

        Just w ->
            div [ class "workout" ]
                [ span [ class "workout__detail_header" ]
                    [ header w
                    , button [ onClick (Activate (List.head w.groups)), class "button" ] [ text "start" ]
                    ]
                , span [ class "workout__groups" ] (List.map (Group.view ag) w.groups)
                ]


header : Workout -> Html msg
header w =
    div []
        [ span [ class "workout__name" ] [ text w.name ]
        , span [ class "workout__description" ] [ text w.description ]
        ]



-- HELPERS


findWorkout : List Workout -> String -> Maybe Workout
findWorkout ws n =
    List.filter (\w -> w.name == n) ws |> List.head



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- SERIALIZATION


decode : D.Decoder Workout
decode =
    D.map3 Workout
        (D.field "name" D.string)
        (D.field "description" D.string)
        (D.field "groups" (D.list Group.decode))



-- CMD


fetchWorkouts : String -> Cmd Msg
fetchWorkouts url =
    Http.get
        { url = url
        , expect = Http.expectJson GotWorkouts (D.list decode)
        }
