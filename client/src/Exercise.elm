module Exercise exposing
    ( Exercise
    , decode
    , view
    )

import Effort
import Html exposing (Html, span, text)
import Html.Attributes exposing (class)
import Json.Decode as D


type alias Exercise =
    { name : String
    , effort : Effort.Effort
    }


view : Exercise -> Html msg
view e =
    let
        classes =
            if e.name == "x-man crunch" then
                "exercise exercise--active"

            else
                "exercise"
    in
    span [ class classes ]
        [ span [ class "exercise__name" ] [ text e.name ]
        , Effort.view e.effort
        , span [ class "exercise__log" ] [ log ]
        ]


log : Html msg
log =
    span [ class "exercise__log" ]
        [ text ("yesterday" ++ " 20kg" ++ " x15") ]


decode : D.Decoder Exercise
decode =
    D.map2 Exercise
        (D.field "name" D.string)
        (D.field "effort" Effort.decode)
