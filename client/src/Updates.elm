module Updates exposing (init, update)

import Browser
import Browser.Navigation as Nav
import Log
import Models exposing (Model)
import Msgs exposing (Msg(..))
import Routes exposing (toRoute)
import Url
import Workout


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    let
        ( wModel, wCmd ) =
            Workout.init "http://localhost:8080/api/workouts"

        ( lModel, lCmd ) =
            Log.init ()
    in
    ( { key = key
      , route = toRoute url
      , workoutsModel = wModel
      , logModel = lModel
      , errors = []
      }
    , Cmd.batch
        [ Cmd.map WorkoutMsg wCmd
        , Cmd.map LogMsg lCmd
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | route = toRoute url }
            , Cmd.none
            )

        WorkoutMsg wMsg ->
            let
                ( updatedModel, updateCmd ) =
                    Workout.update wMsg model.workoutsModel

                errors =
                    model.errors ++ updatedModel.errors
            in
            ( { model | workoutsModel = updatedModel, errors = errors }, Cmd.map WorkoutMsg updateCmd )

        LogMsg lMsg ->
            let
                ( updatedModel, updateCmd ) =
                    Log.update lMsg model.logModel
            in
            ( { model | logModel = updatedModel }, Cmd.map LogMsg updateCmd )
