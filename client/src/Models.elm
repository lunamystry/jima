module Models exposing (Errors, Model)

import Browser.Navigation as Nav
import Log
import Routes exposing (Route)
import Workout


type alias Errors =
    List String


type alias Model =
    { key : Nav.Key
    , route : Route
    , workoutsModel : Workout.Model
    , logModel : Log.Model
    , errors : Errors
    }
