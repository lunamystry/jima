module Effort exposing (Effort(..), decode, view)

import Html exposing (Html, span, text)
import Html.Attributes exposing (class)
import Json.Decode as D


type Effort
    = Reps Int
    | Seconds Int


view : Effort -> Html msg
view e =
    case e of
        Reps value ->
            span [ class "effort" ]
                [ span [ class "effort__value" ] [ text ("x" ++ String.fromInt value) ]
                ]

        Seconds value ->
            span [ class "effort" ]
                [ span [ class "effort__value" ] [ text (String.fromInt value ++ "s") ]
                ]


decode : D.Decoder Effort
decode =
    let
        makeEffort : String -> D.Decoder Effort
        makeEffort t =
            case t of
                "reps" ->
                    D.map Reps (D.field "value" D.int)

                "seconds" ->
                    D.map Seconds (D.field "value" D.int)

                _ ->
                    D.fail <|
                        "Trying to decode effort, but "
                            ++ t
                            ++ " is not known."
    in
    D.field "type" D.string |> D.andThen makeEffort
