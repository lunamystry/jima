module Group exposing
    ( Group
    , decode
    , view
    )

import Effort
import Exercise
import Html exposing (Html, div, span)
import Html.Attributes exposing (class)
import Json.Decode as D


type alias Group =
    { id : Int
    , effort : Effort.Effort
    , exercises : List Exercise.Exercise
    }


view : Maybe Group -> Group -> Html msg
view activeGroup g =
    let
        classes =
            case activeGroup of
                Nothing ->
                    "group "

                Just aGroup ->
                    if g.id == aGroup.id then
                        "group group--active "

                    else
                        "group "
    in
    case g.exercises of
        [ ex ] ->
            div [ class (classes ++ "group--single") ]
                [ Effort.view g.effort
                , Exercise.view ex
                ]

        exes ->
            div [ class (classes ++ "group--multi") ]
                [ Effort.view g.effort
                , span [ class "group__exercises" ] (List.map Exercise.view exes)
                ]


decode : D.Decoder Group
decode =
    D.map3 Group
        (D.field "gid" D.int)
        (D.field "effort" Effort.decode)
        (D.field "exercises" (D.list Exercise.decode))
