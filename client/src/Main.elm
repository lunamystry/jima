module Main exposing (main)

import Browser
import Models exposing (Model)
import Msgs exposing (Msg(..))
import Subscriptions exposing (subscriptions)
import Updates exposing (init, update)
import Views exposing (view)


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
