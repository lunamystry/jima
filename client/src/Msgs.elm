module Msgs exposing (Msg(..))

import Browser
import Log
import Url
import Workout


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WorkoutMsg Workout.Msg
    | LogMsg Log.Msg
