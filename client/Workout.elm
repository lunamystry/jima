module Workout exposing (..)

import Element
    exposing
        ( Element
        , column
        , spacing
        , padding
        , paddingXY
        , width
        , fill
        , el
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Color exposing (..)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Navigation exposing (newUrl)
import List.Verify exposing (notEmpty)
import Verify
import Group exposing (Form(..))
import Helpers
    exposing
        ( Input(..)
        , isEmpty
        , isValid
        , jimaInput
        )


-- TYPES


type Msg
    = NameChange String
    | SubmitForm
    | SaveGroup Group.Form
    | OnAdd (Result Http.Error Workout)
    | OnFetch (Result Http.Error (List Workout))
    | GroupMsg Group.Msg


type alias Model =
    { form : Form
    , workouts : List Workout
    }


type alias Workout =
    { id : Int
    , name : String
    , groups : List Group.Group
    }


type alias Form =
    { name : Input String
    , groups : List Group.Group
    , groupForm : Group.Form
    , errors : List String
    , info : List String
    }


type alias ValidatedForm =
    { name : String
    , groups : List Group.Group
    }


verify : Verify.Validator String Form ValidatedForm
verify =
    Verify.ok ValidatedForm
        |> Verify.verify .name (isEmpty "The name must be given")
        |> Verify.verify .groups (notEmpty "Akuph' ama-set?")



-- INIT


init : ( Model, Cmd Msg )
init =
    ( initialModel, fetchCmd )


initialModel : Model
initialModel =
    { form = initialForm
    , workouts = []
    }


initialForm : Form
initialForm =
    { name = Clean
    , groups = []
    , groupForm = Group.initialForm
    , errors = []
    , info = []
    }



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NameChange value ->
            ( { model
                | form =
                    model.form
                        |> updateName value
              }
            , Cmd.none
            )

        SubmitForm ->
            case verify model.form of
                Ok validatedForm ->
                    ( { model | form = initialForm }, addCmd validatedForm )

                Err errors ->
                    let
                        form =
                            model.form

                        formWithError =
                            { form | errors = errors }
                    in
                        ( { model | form = formWithError }, Cmd.none )

        OnAdd result ->
            case result of
                Ok workout ->
                    ( { model | workouts = (model.workouts ++ [ workout ]) }, newUrl "#/workouts" )

                Err error ->
                    let
                        form =
                            model.form

                        formWithError =
                            { form | errors = [ toString error ] }
                    in
                        ( { model | form = formWithError }, Cmd.none )

        OnFetch result ->
            case result of
                Ok workouts ->
                    ( { model | workouts = workouts }, Cmd.none )

                Err error ->
                    let
                        form =
                            model.form

                        formWithError =
                            { form | errors = [ toString error ] }
                    in
                        ( { model | form = formWithError }, Cmd.none )

        GroupMsg msg ->
            let
                groupForm =
                    Group.update msg model.form.groupForm

                updateForm form =
                    { form | groupForm = groupForm }
            in
                ( { model | form = updateForm model.form }, Cmd.none )

        SaveGroup groupForm ->
            let
                f =
                    saveGroup groupForm model.form
            in
                ( { model | form = f }, Cmd.none )


saveGroup : Group.Form -> Form -> Form
saveGroup groupForm form =
    case Group.verify groupForm of
        Ok group ->
            { form | groupForm = Group.initialForm, groups = form.groups ++ [ group ] }

        Err errors ->
            { form | errors = form.errors ++ errors }


updateName : String -> Form -> Form
updateName value form =
    if String.isEmpty value then
        { form | name = Invalid "An exercise has to have a name" }
    else
        { form | name = Valid value }


updateGroup : Group.Group -> Form -> Form
updateGroup group form =
    { form | groups = group :: form.groups }



-- VIEW


list : List Workout -> Element Msg
list workouts =
    case workouts of
        [] ->
            Element.text "no workouts"

        all ->
            column [ spacing 10 ]
                (List.map card all)


card : Workout -> Element Msg
card workout =
    column [ Border.innerGlow lightCharcoal 2, padding 10 ]
        [ el [ Font.size 30, paddingXY 0 10 ] (Element.text workout.name)
        , Element.map GroupMsg <| Group.list workout.groups
        ]


choose : Element Msg
choose =
    Element.text "What do you wanna do?"


errors : List String -> Element Msg
errors errorList =
    column []
        (List.map Element.text errorList)


add : Form -> Element Msg
add form =
    let
        ( onAddSet, addButtonTextColor ) =
            if isValid Group.verify form.groupForm then
                ( Just (SaveGroup form.groupForm), white )
            else
                ( Nothing, lightGrey )

        ( onSave, saveButtonTextColor ) =
            if isValid verify form then
                ( Just SubmitForm, white )
            else
                ( Nothing, lightGrey )
    in
        column
            [ spacing 10
            , padding 10
            ]
            [ errors form.errors
            , jimaInput [] "Name" form.name NameChange (\v -> v)
            , Element.map GroupMsg <| Group.add form.groupForm
            , Element.map GroupMsg <| Group.list form.groups
            , Input.button
                [ Background.color lightBlue
                , Border.rounded 3
                , padding 10
                , width fill
                , Font.color addButtonTextColor
                ]
                { onPress = onAddSet
                , label = Element.text "add set"
                }
            , Input.button
                [ Background.color lightBlue
                , Border.rounded 3
                , padding 10
                , width fill
                , Font.color saveButtonTextColor
                ]
                { onPress = onSave
                , label = Element.text "save workout"
                }
            ]



-- URLs


workoutsUrl : String
workoutsUrl =
    "http://localhost:4000/workouts"



-- COMMANDS


addCmd : ValidatedForm -> Cmd Msg
addCmd form =
    addRequest form
        |> Http.send OnAdd


addRequest : ValidatedForm -> Http.Request Workout
addRequest form =
    Http.request
        { body = encode form |> Http.jsonBody
        , expect = Http.expectJson decode
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = workoutsUrl
        , withCredentials = False
        }


fetchCmd : Cmd Msg
fetchCmd =
    Decode.list decode
        |> Http.get workoutsUrl
        |> Http.send OnFetch



-- SERIALIZATION


encode : ValidatedForm -> Encode.Value
encode f =
    let
        attributes =
            [ ( "name", Encode.string f.name )
            , ( "sets", Encode.list (List.map Group.encode f.groups) )
            ]
    in
        Encode.object attributes


decode : Decode.Decoder Workout
decode =
    Decode.map3 Workout
        (Decode.field "id" Decode.int)
        (Decode.field "name" Decode.string)
        (Decode.field "sets" (Decode.list Group.decode))
