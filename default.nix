{ pkgs ? import <nixpkgs> {} }:
with pkgs;
with haskellPackages;

let
  jima-server = import ./server/default.nix {};
  jima-client = import ./client/default.nix {};
in 
stdenv.mkDerivation rec {
  name = "jima";
  src = ./.;
  buildInputs = [
    pkgs.zlib
    jima-server
    jima-client
  ];
  installPhase = ''
    mkdir -p $out/
    cp -r ${jima-server}/bin/* $out
    cp -r ${jima-client}/* $out
  '';
}
